var questionnaire = 
`<div id="questionsView">
	<h2>Digitale Themen bei der Europawahl 2019</h2>
	<p>
		Einfach jeweils »stimme zu«, »neutral« oder »stimme nicht zu« auswählen und erfahren, wie die Parteien dazu
		abgestimmt haben und mit welchen die Übereinstimmung am größten ist:
	</p>

	<form id="questions" method="post" action="javascript:;"></form>

	<button onclick="calc()" id="calculate">Übereinstimmung berechnen</button>
	<div id="result"></div>
</div>


<script id="tmpl-questions" type="x-tmpl-mustache">
	{{#question}}
	<div class="question-wrapper" data-q="{{id}}" id="q{{id}}">
		<div class="question-title">
			<span class="question-big-number">{{num}}</span>
			<h3 class="question-label"><span class="question-small-number">{{num}}</span> {{label}}</h3>

			<p>{{&question}}
				{{#anyInfo}}
					<p>
						<button class="more-info-button">Mehr Info</button>
						<button class="less-info-button">Info zuklappen</button>
					</p>
					
					<div class="questions-info">
						{{#terms}}
							<p><b>{{#term}}{{term}} {{/term}}</b>{{&explanation}}</p>
						{{/terms}}

						{{#hasInfotext}}
							<p class="margin-backgroundinfo"><b>Weitere Hintergrundinformationen</b></p>
						{{/hasInfotext}}

						{{#links}}
							{{#link}}
							<p>
								<a class="questions-info-link" href="{{link}}" target="_blank">
									{{#title}}{{title}}{{/title}}
									{{^title}}Abstimmung auf votewatch{{/title}}
									{{#date}}({{date}}){{/date}}
								</a>
							</p>
							{{/link}}
						{{/links}}

						{{#infotext}}
							{{#link}}
								<p>
									<a class="questions-info-link" href="{{link}}" target="_blank">
										{{#title}}{{title}}{{/title}}
										{{^title}}{{link}}{{/title}}
										{{#date}}({{date}}){{/date}}
									</a>
								</p>
							{{/link}}
							{{^link}}
								{{#title}}
									<p>{{title}}</p>
								{{/title}}
							{{/link}}
						{{/infotext}}
					</div>
				{{/anyInfo}}
			</p>
		</div>

		<div id="radio-button-wrapper" class="radio-button-wrapper">
			<div class="radio-button-group">
				<input type="radio" id="q{{id}}a0" name="q{{id}}a" value="2">
				<label for="q{{id}}a0">
					<span class="radio-button-group-label">stimme zu</span>
				</label>
			</div>
			<div class="radio-button-group">
				<input type="radio" id="q{{id}}a1" name="q{{id}}a" value="1">
				<label for="q{{id}}a1">
					<span class="radio-button-group-label">neutral</span>
				</label>
			</div>
			<div class="radio-button-group">
				<input type="radio" id="q{{id}}a2" name="q{{id}}a" value="0">
				<label for="q{{id}}a2">
					<span class="radio-button-group-label">stimme nicht zu</span>
				</label>
			</div>
		</div>

	</div>
	{{/question}}
</script>

<script id="tmpl-result" type="x-tmpl-mustache">
	<div id="comparison">
		<h2>Ihr Ergebnis im Überblick</h2>
		<p>Die folgenden Prozentzahlen geben an, zu wieviel Prozent Ihre Position mit dem Abstimmungsverhalten der jeweiligen Partei übereinstimmt.</p>
		<ol class="comparison-wrapper">
		{{#comparison}}
			<li>
				<h4 title="{{party_long}}">{{party_short}}</h4>
				<p class="percentage">
					<span class="percentage-bar" style="width: {{width}}%;"></span>
					<span class="percentage-label">{{percent}} %</span>
				</p>
			</li>		
		{{/comparison}}
		</ol>
	</div>
	<div id="detail">
		<h2>Das Abstimmungsverhalten der Parteien im Detail</h2>
		<ul class="detail-table">
		{{#detail}}
			<li id="detail-{{id}}">
				<h3><span>{{num}}</span> {{label}}</h3>
				<p>{{&question}}</p>
				<ul class="answers">
					{{#answers}}
						<li class="answer{{#selected}} selected{{/selected}}">
							<h4 class="type-{{answer_type}}">{{answer_label}}</h4>
							{{#selected}}
								<span class="you">Ihre Position</span>
							{{/selected}}
							<ul class="parties">
								{{#parties}}
									<li class="party explanation">
										<h5 title="{{party_long}}">{{party_short}} <i tabindex="0" class="icon-down" title="Erläuterung"></i></h5>
										<blockquote class="blockquote-color">
											Abgeordnete: {{delegates}}
											<canvas class="donut-chart-margin"
												data-for={{results.for}}
												data-against={{results.against}}
												data-abstained={{results.abstained}}
												data-absent={{results.absent}}
												data-didntvote={{results.didntvote}}
											></canvas>
										</blockquote>
									</li>
								{{/parties}}
								{{^parties}}
									<li class="party no-party">
										<em>Keine Partei vertrat mehrheitlich diese Position.</em>
									</li>
								{{/parties}}
							</ul>
						</li>
					{{/answers}}
				</ul>
			</li>
		{{/detail}}
		</ul>
	</div>
</script>`
