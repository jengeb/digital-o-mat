var introPage = 
`<div id="intro">
	<div class="row">
		<h2>Digital-O-Mat</h2>
			<h3>Europawahl 2019</h3>
			<p>Welche Partei macht wirklich welche digitale Politik?</p>
			<p>
				Versprochen wird im Wahlkampf viel – am 26. Mai 2019 wollen deutsche Parteien ins Europaparlament gewählt werden. 
				<b>Der Digital-O-Mat zeigt auf einen Blick, wie die Parteien in der Vergangenheit tatsächlich abgestimmt haben</b> zur 
				Freiheit im Internet, zu Datenschutz, Überwachung und mehr.
				<!--Versprochen wird im Wahlkampf viel. Aber wie haben die Parteien, die am 26. Mai 2019 gewählt werden wollen, in der
				Vergangenheit zur Freiheit im Internet, zu Datenschutz und Überwachung tatsächlich abgestimmt? Der Digital-O-Mat
				gibt die Antwort!-->
			</p>
			<noscript>
				<h2>Bitte JavaScript aktivieren</h2>
				<p class="noscript-message">
					Der Digital-O-Mat benötigt JavaScript.
				</p>
			</noscript>
			<button href="/questionnaire" onclick="onNavItemClick('/questionnaire'); return false;" id="activate">Jetzt Abstimmungsverhalten vergleichen!</button>
			<p>
				Der Digital-O-Mat zur Europawahl 2019 lässt Sie auswählen, wie Sie bei zehn
				Schlüssel-Abstimmungen im Europäischen Parlament zwischen 2014 und 2019 abgestimmt hätten, und vergleicht dies mit
				dem tatsächlichen Abstimmungsverhalten der in Deutschland bei der Europawahl 2019 wählbaren Parteien.
			</p>
			<p>
				Der Digital-O-Mat wird von einer Gruppe von NGOs zur Verfügung gestellt, die sich mit Netzthemen befassen und für
				digitale Freiheitsrechte eintreten. Details zu uns siehe „Über uns“. Zurück zu dieser Startseite gelangen Sie jederzeit per Klick auf das Logo oben links.
			</p>
		</div>
		<div class="row">
			<img class="image-margin" src="../assets/images/Digital-o-Mat_Europakarte_v03.svg" alt="Europakarte">
		</div>
	</div>
</div>`
