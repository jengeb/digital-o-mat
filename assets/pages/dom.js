var domPage = 
`<div id="aboutDoM">
	<h2>Informiert eine Wahlentscheidung treffen</h2>
	<p>
		Mit Hilfe dieses Digital-O-Maten können Sie das tatsächliche Abstimmungsverhalten der Abgeordneten deutscher
		Parteien im Europaparlament mit Ihren eigenen Positionen vergleichen. Dafür haben wir zehn Kernfragen der Digitalisierung 
		ausgewählt, zu denen zwischen 2014 und 2019 im Europaparlament Abstimmungen stattfanden.
	</p>
	<h3>So funktioniert der Digital-O-Mat</h3>
	<p>
		Sie können sich auf dieser Website per Klick zu netzpolitischen Themen zustimmend, neutral oder ablehnend positionieren.
		Der Digital-O-Mat vergleicht anschließend diese von Ihnen gewählten Positionen mit dem tatsächlichen Abstimmungsverhalten der
		Parteien während der gerade endenden Legislaturperiode. Zusätzlich erklärt die Seite die Streitthemen kurz und bietet weiterführende Informationen.
	</p>
	<p>
		In der Auswertung wird außerdem detaillierter aufgeschlüsselt, wie viele Abgeordnete einer Partei bei den jeweiligen
		Abstimmungen zugestimmt, sich enthalten oder dagegen gestimmt haben (Klick auf das Kreisdiagramm).
	</p>
	<p>
		Es kann vorkommen, dass Sie mit mehreren Parteien zum gleichen Prozentsatz übereinstimmen. In diesem Fall wird die
		Auflistung dieser Parteien absteigend nach deren Wahlergebnis 2014 angezeigt.
	</p>
	<h3>Was der Digital-O-Mat kann und was nicht</h3>
	<p>
		Als zusätzliche Informationsquelle unterstützt der Digital-O-Mat Wählerinnen und Wähler dabei, sich einen Überblick zu
		verschaffen. Er ist als möglichst neutrale Informationsquelle gedacht.
	</p>
	<p>
		Anders als bisherige Ausgaben bildet dieser Digital-O-Mat keine Wahlversprechen der Parteien oder deren Parteiprogramme ab, 
		sondern gibt einen retrospektiven Überblick zu zehn Themen, die im Europaparlament bereits diskutiert und entschieden wurden. Enthalten sind nur Informationen zum
		Abstimmungsverhalten deutscher Parteien, die im Zeitraum zwischen 2014 und 2019 im EU-Parlament vertreten waren.
	</p>					
	<p>
		<b>Wichtig:</b> Das Abstimmungsergebnis sagt nicht zwingend etwas darüber aus, warum auf die eine oder die andere Weise
		abgestimmt wurde. Eine Partei kann beispielsweise ein Gesetz ablehnen, weil es zu viel Überwachung für die Bevölkerung
		bedeutet. Eine andere Partei kann das selbe Gesetz aber ebenfalls ablehnen, weil sie der Auffassung ist, dass die
		Überwachung nicht weit genug geht.
	</p>
	<p>
		Also: <b>Schauen Sie sich die Parteien und ihre Kandidatinnen und Kandidaten ganz genau an!</b> Informieren Sie sich gezielt über Abstimmungen, zum
		Beispiel auf <a target="_blank" href="https://www.votewatch.eu/en/term8-european-parliament-members.html">VoteWatch.eu</a>. Kontaktieren Sie
		Abgeordnete und Parteien, wenn Sie konkrete Fragen haben. Einige weiterführende Quellen zu den jeweiligen Themen
		haben wir direkt im Digital-O-Mat verlinkt.
	</p>
	<h3>Über den Datensatz</h3>
	<p>
		Alle verwendeten Abstimmungsergebnisse sind <a target="_blank" href="https://www.votewatch.eu/">VoteWatch.eu</a> entnommen.
	</p>
</div>`