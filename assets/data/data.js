var data = {
	"questions": [{
			"id": 1,
			"name": "Uploadfilter in der Urheberrechtsreform",
			"title": "Uploadfilter",
			"description": "Online-Plattformen sollen auch für die von ihren Usern hochgeladenen Filme, Bilder, Musikstücke und Texte Lizenzen bei den jeweiligen Rechteinhabern erwerben müssen. Wollen sie dies nicht, müssen sie stattdessen auf Wunsch der Rechteinhaber alle rechtswidrigen User-Uploads verhindern, etwa mittels technischer Systeme, die geschützte Werke erkennen und ihren Upload automatisch unterbinden können.",
			"terms": [{
				"term": "Uploadfilter",
				"explanation": "meint in der gegenwärtigen polistischen Debatte üblicherweise automatische Systeme, die sämtliche auf eine Plattform durch User hochgeladene Mediendateien mit einer Referenzdatenbank vergleichen. In einer solchen Referenzdatenbank sind Erkennungsmerkmale urheberrechtlich geschützter Werke gespeichert. Findet das System bei der Durchleuchtung hochgeladener Mediendateien in einer davon ausreichend viele Merkmale eines Werkes aus der Referenzdatenbank wieder, meldet es dies als positiven Fund. Für diesen Fall können verschiedene Folgen im System vordefiniert sein: Die Mediendatei kann etwa mit einer Lizenzdatenbank abgeglichen und bei Vorliegen einer Lizenz normal durchgelassen werden. Ebenso ist definierbar, dass sie gelöscht oder zunächst nur ihr Upload blockiert und sie zur händischen Prüfung jemandem vorgelegt wird. Zu beachten ist: Um mittels eines solchen Uploadfilter-Systems das Hochladen spezifischer Werke sicher zu unterbinden, ist stets ein Durchleuchten sämtlicher Uploads der betreffenden Plattform erforderlich und reicht eine Stichprobenkontrolle nicht aus. Zudem sind bislang keine Uploadfilter-Systeme bekannt, die über die Erkennung von Werken in Uploads hinaus auch bewerten könnten, ob ein bestimmtes Werk im konkreten Nutzungskontext auch ohne Erlaubnis der jeweiligen Rechteinhaber genutzt werden darf, etwa aufgrund einer gesetzlichen Regelung zugunsten von Zitaten, Parodien, Berichterstattung usw.",
				"link": null
			}],
			"votewatch": [{
				"title": null,
				"link": null,
				"date": null
			}],
			"background": [{
					"title": "iRights.info: „Webschau: EU-Urheberrechtsreform und Artikel 13 – pro und kontra“",
					"link": "https://irights.info/webschau/webschau-eu-urheberrechtsreform-und-artikel-13-pro-und-kontra/29417",
					"date": null
				},
				{
					"title": "Wikipedia-Artikel: „YouTube“-Abschnitt über das dortige Filtersystem „Content-ID“",
					"link": "https://de.wikipedia.org/wiki/YouTube#Content-ID",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 28,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 22,
							"abstained": 3,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 7,
							"abstained": 0,
							"absent": 0,
							"didntvote": 2
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 7,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 5,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}
			]
		},
		{
			"id": 2,
			"name": "ePrivacy",
			"title": "Web-Tracking (ePrivacy)",
			"description": "Anbieter von Websites sollen das Betrachten von Inhalten <b>nicht</b> davon abhängig machen dürfen, ob Nutzende einer Verfolgung, Aufzeichnung und Auswertung ihres Verhaltens (Tracking) für Werbezwecke zustimmen.",
			"terms": [{
					"term": "ePrivacy",
					"explanation": "steht kurz für die EU-Verordnung über die Achtung des Privatlebens und den Schutz personenbezogener Daten in der elektronischen Kommunikation. Diese soll unter anderem Chat-Dienste, Instant Messenger, Tracking, Verschlüsselung sowie Vorgaben für Browser und Software regulieren.",
					"link": null
				},
				{
					"term": "Web-Tracking",
					"explanation": "bedeutet, dass Websitebetreiber oder Dritte Daten darüber erheben, welche Seiten eine Nutzerin bzw. ein Nutzer aufruft, um diese Daten zu Werbezwecken zu verarbeiten.",
					"link": null
				}
			],
			"votewatch": [{
				"title": null,
				"link": "https://www.votewatch.eu/en/term8-respect-for-private-life-and-the-protection-of-personal-data-in-electronic-communications-and-repeal.html",
				"date": null
			}],
			"background": [{
					"title": "EDRi: „e-Privacy: What happened and what happens next“",
					"link": "https://edri.org/e-privacy-what-happened-and-what-happens-next/",
					"date": "27.11.2017"
				},
				{
					"title": "Frauenhofer Gesellschaft: „Was bedeutet Web-Tracking?“",
					"link": "https://www.sit.fraunhofer.de/de/track-your-tracker/worum-gehts/was-bedeutet-web-tracking/",
					"date": null
				},
				{
					"title": null,
					"link": "https://crackedlabs.org/",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 28,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 22,
							"against": 0,
							"abstained": 0,
							"absent": 4,
							"didntvote": 1
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 6,
							"against": 0,
							"abstained": 0,
							"absent": 5,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 4,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 1,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				}
			]
		},
		{
			"id": 3,
			"name": "Passenger Name Record (PNR), zu deutsch: Fluggastdatensatz",
			"title": "Fluggastdatensatz (Passenger Name Record (PNR))",
			"description": "Umfangreiche Informationen (z. B. Name, Adressdaten, Zahlungsdetails, Reisebüro und Sachbearbeiter, Buchungsverhalten, Essenswünsche und Sitzplatz) über die Passagiere jedes Flugs in die EU und aus der EU sollen fünf Jahre lang auf Vorrat gespeichert werden.",
			"terms": [{
				"term": "PNR ",
				"explanation": "steht für Passenger Name Record, deutsch: Fluggastdatensatz. Es umfasst, anders als der Name vermuten lässt, nicht nur die Namen und Stammdaten der Fluggäste, sondern auch viele weitere Daten – etwa welche Tickets zusammen gebucht wurden, mit welcher Kreditkarte sie bezahlt wurden, und ob bestimmte Essenswünsche angegeben wurden.",
				"link": null
			}],
			"votewatch": [{
				"title": "Abstimmung auf votewatch",
				"link": "https://www.votewatch.eu/en/term8-use-of-passenger-name-record-data-eu-pnr-draft-legislative-resolution-vote-commission-proposal-ordin.html",
				"date": "14.04.2016"
			}],
			"background": [{
					"title": null,
					"link": "https://edri.org/ep-pushes-for-more-surveillance-and-profiling-of-eu-citizens/",
					"date": null
				},
				{
					"title": null,
					"link": "https://de.wikipedia.org/wiki/Passenger_Name_Record",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 25,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 1
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 12,
							"against": 12,
							"abstained": 1,
							"absent": 1,
							"didntvote": 1
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 10,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 6,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 4,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 4,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}
			]
		},
		{
			"id": 4,
			"name": "Privacy Shield",
			"title": "US-EU-Datenabkommen (Privacy Shield)",
			"description": "Persönliche Daten aus der EU <b>dürfen nicht</b> auf Grundlage der „Privacy Shield“-Vereinbarung in die USA übermittelt werden, weil die USA das EU-Datenschutz-Niveau nicht einhalten.",
			"terms": [{
				"term": "Das sogenannte Privacy Shield",
				"explanation": "ist ein Abkommen zwischen der EU und den USA, das bei Datentransfer aus den EU-Mitgliedsstaaten in die USA einen Datenschutz auf EU-Niveau garantieren soll. Kritik an der Wirksamkeit gibt es unter anderem von der Artikel-29-Datenschutzgruppe (siehe <a taget='_blank' href='https://ec.europa.eu/newsroom/just/document.cfm?doc_id=48782'>PDF</a>) (alle europäischen Datenschutzaufsichtsbehörden), die beispielsweise bemängelt, dass das Abkommen lediglich auf Versprechen der US-Regierung basiert, aber nicht auf US-Recht.",
				"link": null
			}],
			"votewatch": [{
				"title": null,
				"link": "https://www.votewatch.eu/en/term8-adequacy-of-the-protection-afforded-by-the-eu-us-privacy-shield-motion-for-resolution-vote-resolutio-2.html",
				"date": "05.07.2018"
			}],
			"background": [{
					"title": "EU-US Privacy Shield erklärt beim Bayerische Landesbeauftragte für den Datenschutz",
					"link": "https://www.datenschutz-bayern.de/0/privacy_shield/privacy_shield.html",
					"date": null
				},
				{
					"title": "netzpolitik.org: „EU-Parlament stellt sich gegen Mechanismus zum transatlantischen Datenaustausch“",
					"link": "https://netzpolitik.org/2018/eu-parlament-stellt-sich-gegen-mechanismus-zum-transatlantischen-datenaustausch/",
					"date": null
				},
				{
					"title": "Wikipedia-Eintrag zu „Privacy Shield“",
					"link": "https://de.wikipedia.org/wiki/EU-US_Privacy_Shield",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 20,
							"abstained": 0,
							"absent": 3,
							"didntvote": 4
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 23,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 4
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 7,
							"against": 0,
							"abstained": 0,
							"absent": 2,
							"didntvote": 2
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 2,
							"didntvote": 2
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 1,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 5,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}
			]
		},
		{
			"id": 5,
			"name": "Netzneutralität (Telekommunikationsbinnenmarktordnung)",
			"title": "Netzneutralität",
			"description": "Telekommunikationsanbieter dürfen bestimmte Daten (z.B. Musik-, Video- und Gaming-Dienste, Nachrichten-Plattformen, Messenger etc.) <b>nicht</b> bevorzugen oder benachteiligen.",
			"terms": [{
				"term": "Netzneutralität",
				"explanation": "bedeutet die Gleichbehandlung aller Daten. Internetanbieter stehen vor der Frage, wie sie mit immer größeren Datenmengen umgehen sollen. Es gibt grundsätzlich zwei Möglichkeiten: Werden alle Daten bei der Übertragung gleich behandelt, kann von Netzneutralität gesprochen werden. Werden die Daten bestimmter Dienste schneller und/oder in besserer Qualität übertragen, werden dabei andere Datenströme diskriminiert.",
				"link": null
			}],
			"votewatch": [{
				"title": null,
				"link": "https://www.votewatch.eu/en/term8-european-single-market-for-electronic-communications-draft-legislative-resolution-article-2-paragrap.html",
				"date": null
			}],
			"background": [{
					"title": "Chaos Computer Club erklärt Netzneutralität",
					"link": "https://www.ccc.de/en/netzneutralitaet",
					"date": null
				},
				{
					"title": "epicenter.works erklärt Netzneutralität",
					"link": "https://epicenter.works/thema/netzneutralitaet",
					"date": null
				},
				{
					"title": "Wikipedia-Eintrag zu Netzneutralität",
					"link": "https://de.wikipedia.org/wiki/Netzneutralit%C3%A4t#Positionen",
					"date": null
				},
				{
					"title": "European Digital Rights: „A system you never heard of undermined net neutrality“",
					"link": "https://edri.org/system-you-never-heard-of-undermined-net-neutrality/",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 26,
							"abstained": 0,
							"absent": 1,
							"didntvote": 1
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 22,
							"against": 1,
							"abstained": 1,
							"absent": 0,
							"didntvote": 3
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 10,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 7,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 5,
							"abstained": 1,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 4,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": null,
							"against": null,
							"abstained": null,
							"absent": null,
							"didntvote": null
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}
			]
		},
		{
			"id": 6,
			"name": "Auswirkungen von Massenüberwachungsprogrammen",
			"title": "Auswirkungen von Massenüberwachungsprogrammen",
			"description": "Die Verhandlungen mit den USA über ein Handels- und Investitionsschutz-Abkommen (TTIP) sollten ausgesetzt werden; mindestens solange, wie die USA an ihren Programmen zur Massenüberwachung (z.B. PRISM, Echelon etc.) festhalten.",
			"terms": [{
					"term": "TTIP",
					"explanation": "ist eine geplante Transatlantische Handels- und Investitionspartnerschaft zwischen der Europäischen Union und den USA. Kritik gibt es an der Intransparenz des Verhandlungsprozesses sowie an Umwelt-, Sozial- und Rechtsstaats-Standards.",
					"link": null
				},
				{
					"term": "PRISM",
					"explanation": "ist ein US-Programm zur Überwachung und Auswertung von Daten. Die Abkürzung steht für: <b>P</b>lanning tool for <b>R</b>esource <b>I</b>ntegration, <b>S</b>ynchronization, and <b>M</b>anagement (deutsch: „Planungswerkzeug für Ressourcenintegration, Synchronisation und Management“).",
					"link": null
				},
				{
					"term": "Echelon",
					"explanation": "ist ein Spionagenetz, das von unterschiedlichen Nachrichtendiensten betrieben wird. Es dient dem Abhören bzw. der Überwachung von privaten sowie geschäftlichen Telefongesprächen, Internet-Daten und Faxverbindungen. Die Auswertung der abgegriffenen Daten erfolgt vollautomatisch.",
					"link": null
				}
			],
			"votewatch": [{
				"title": null,
				"link": "https://www.votewatch.eu/en/term8-follow-up-to-the-european-parliament-resolution-of-12-march-2014-on-the-electronic-mass-surveillance-13.html",
				"date": null
			}],
			"background": [{
				"title": "Pressemitteilung des Europäischen Parlaments vom 12.03.2014",
				"link": "http://www.europarl.europa.eu/news/de/press-room/20140307IPR38203/parlament-droht-mit-konsequenzen-falls-usa-massenuberwachung-nicht-einstellt",
				"date": null
			}],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 27,
							"abstained": 0,
							"absent": 1,
							"didntvote": 1
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 19,
							"abstained": 4,
							"absent": 0,
							"didntvote": 4
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 8,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 6,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}				
			]
		},
		{
			"id": 7,
			"name": "ETIAS (Europäisches Reiseinformations- und Autorisierungssystem)",
			"title": "ETIAS (Europäisches Reiseinformations- und Autorisierungssystem)",
			"description": "Menschen, die aus Nicht-EU-Staaten einreisen möchten, sollen, sofern sie kein Visum benötigen, eine Reisegenehmigung (ETIAS Genehmigung) einholen müssen. Die erhobenen Daten sollen durch einen Algorithmus auf Risikoindikatoren, darunter Angaben wie Alter, Geschlecht, Nationalität, Gesundheitszustand und vorherige Reisen, untersucht und gegen diverse Datenbanken abgeglichen werden.",
			"terms": [{
					"term": "EITIAS (Reise-)Genehmigung",
					"explanation": "ist das Antragsformular zur Einreise in die Europäische Union umfasst Vorname, Familienname, Geburtsname, -datum und -ort, Staatsangehörigkeit, Adresse, E-Mailadresse und Telefonnummer, Ausbildung und Arbeitserfahrung, Fragen zu Berechtigung und Hintergrund, die auf den Gesundheitszustand, Reisen in Kriegsgebiete oder Regionen, in denen der Antragsteller  abgeschoben oder ausgewiesen wurde, oder Strafregister abzielen. Außerdem müssen Familienmitglieder von EU-Bürgern aus einem anderen Land Beweise für die Beziehung, die Aufenthaltsgenehmigung und weitere Hintergrundinformationen einreichen.",
					"link": null
				},
				{
					"term": "Datenbanken:",
					"explanation": "Die im ETIAS-Antrag gemachten Angaben werden mit diversen behördlichen Datenbanken abgeglichen. Darunter auch eine extra für das ETIAS geschaffene Europol-Watchlist.",
					"link": null
				},
				{
					"term": "Risikoindikatoren:",
					"explanation": "Die Genehmigung zur Einreise wird nicht ausschließlich Kriminellen verwehrt werden. Sie kann beispielsweise auch dann verweigert werden, wenn man einer Gruppe (Alter, Geschlecht, Nationalität etc.) angehört, die für den entsprechenden Reisezeitraum bereits viele Genehmigungen erhalten hat.",
					"link": null
				}
			],
			"votewatch": [{
				"title": "Abstimmung auf votewatch",
				"link": "https://www.votewatch.eu/en/term8-european-travel-information-and-authorisation-system-etias-draft-legislative-resolution-provisional-.html",
				"date": "05.07.2018"
			}],
			"background": [{
					"title": "EU Law Analysis: „Brave new world? the new EU law on travel authorisation for non-EU citizens“",
					"link": "https://eulawanalysis.blogspot.com/2018/04/brave-new-world-new-eu-law-on-travel.html",
					"date": "26.04.2018"
				},
				{
					"title": "Wikipedia: „Europäisches Reiseinformations- und ‑genehmigungssystem“",
					"link": "https://de.wikipedia.org/wiki/Europ%C3%A4isches_Reiseinformations-_und_%E2%80%91genehmigungssystem",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 26,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 0
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 26,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 8,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 5,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 1,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 5,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}				
			]
		},
		{
			"id": 8,
			"name": "Edward Snowden",
			"title": "Edward Snowden",
			"description": "Das EU-Parlament soll die EU-Staaten dazu auffordern, jegliche strafrechtlichen Vorwürfe gegen Edward Snowden fallen zu lassen, ihm Schutz zu gewähren und somit seine Auslieferung oder Überstellung durch Dritte zu verhindern.",
			"terms": [{
				"term": "Edward Snowden",
				"explanation": "ist ein ehemaliger CIA-Mitarbeiter. Im Sommer 2013 informierten seine Enthüllungen die Öffentlichkeit über weltweite Überwachung- und Spionage durch Geheimdienste aus den USA, Großbritannien, Australien, Kanada und Neuseeland. Snowden war mehrfach für den Friedensnobelpreis nominiert.",
				"link": null
			}],
			"votewatch": [{
				"title": "Abstimmung auf votewatch",
				"link": " https://www.votewatch.eu/en/term8-follow-up-to-the-european-parliament-resolution-of-12-march-2014-on-the-electronic-mass-surveillance.html",
				"date": "29.10.2015"
			}],
			"background": [{
					"title": null,
					"link": "http://www.europarl.europa.eu/news/de/press-room/20151022IPR98818/elektronische-massenuberwachung-rechte-der-eu-burger-noch-immer-gefahrdet",
					"date": null
				},
				{
					"title": null,
					"link": "https://de.wikipedia.org/wiki/Edward_Snowden",
					"date": null
				},
				{
					"title": null,
					"link": "https://www.theatlantic.com/international/archive/2015/10/european-parliament-edward-snowden/413257/",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 27,
							"abstained": 0,
							"absent": 1,
							"didntvote": 1
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 23,
							"against": 0,
							"abstained": 2,
							"absent": 0,
							"didntvote": 2
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 8,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 6,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 3,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}				
			]
		},
		{
			"id": 9,
			"name": "Elektronisches Geld",
			"title": "Anonymes Bezahlen mit elektronischem Geld",
			"description": "Die Möglichkeit, mit E-Geld auf Guthabenkarten anonym im Internet einkaufen zu können, wird auf Kleinbeträge bis 150 Euro begrenzt.",
			"terms": [{
				"term": "Elektronisches Geld, beziehungsweise E-Geld",
				"explanation": "wird definiert in <a target='_blank' href='https://eur-lex.europa.eu/legal-content/DE/ALL/?uri=CELEX:32009L0110'>Richtlinie 2009/110/EG</a> als elektronisch, auch magnetisch, gespeicherter monetärer Wert, der gegen Zahlung eines Geldbetrags ausgestellt wird, um damit Zahlungsvorgänge durchzuführen. In der Praxis sind damit Prepaid-Karten und Prepaid-Guthaben gemeint. Mit der Änderung der <a target='_blank' href='https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=celex%3A32015L0849'>EU-Richtlinie 849</a> von 2015 wurde 2018 mit der <a target='_blank' href='https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32018L0843'>5. Richtlinie zu Geldwäsche und Terrorismusfinanzierung (AMLD 5)</a> der Schwellwert zur Ermittlung der Inhaber von Prepaid-Karten von 250 auf 150 Euro herabgesetzt.",
				"link": null
			}],
			"votewatch": [{
				"title": "Abstimmung auf votewatch",
				"link": "https://www.votewatch.eu/en/term8-prevention-of-the-use-of-the-financial-system-for-the-purposes-of-money-laundering-or-terrorist-fina.html",
				"date": null
			}],
			"background": [{
				"title": "Pressemitteilung des Europäischen Parlaments",
				"link": "http://www.europarl.europa.eu/news/de/press-room/20180411IPR01527/geldwasche-bekampfung-offenlegung-der-wahren-eigentumer-von-unternehmen",
				"date": "19.04.2018"
			}],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 27,
							"against": 0,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 22,
							"against": 0,
							"abstained": 0,
							"absent": 3,
							"didntvote": 2
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 11,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 6,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 4,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 1,
							"absent": 0,
							"didntvote": 0
						}
					}
				}			
			]
		},
		{
			"id": 10,
			"name": "Chancengerechtigkeit in der digitalen Bildung",
			"title": "Digitale Bildung",
			"description": "Die Europäische Union soll Chancengerechtigkeit in der Entwicklung digitaler Kompetenzen zwischen Geschlechtern, Generationen und Menschen unterschiedlicher sozialer und regionaler Herkunft aktiv fördern. Die Förderung soll zu diesem Zweck leicht zugängliche, bedarfsorientierte Qualifizierungs- und  Finanzierungsmöglichkeiten umfassen.",
			"terms": [{
				"term": "Digitale Kompetenzen/Digital Literacies",
				"explanation": "sind notwendige Voraussetzungen, um in einer digital geprägten Welt souverän zu handeln und teilzuhaben. Sie umfassen Fähigkeiten wie: Daten und Informationen zu finden, zu hinterfragen, zu kontextualisieren oder zu bewerten; sich mit anderen auszutauschen, gemeinsam mit anderen Inhalte zu kreieren und an gesellschaftlichen Diskursen teilzuhaben. Zugleich geht es darum, ein Bewusstsein für die sich konstant ändernden Funktionsweisen digitaler Technologien zu entwickeln und kreativ Lösungen für individuelle Probleme und Herausforderungen zu finden.",
				"link": null
			}],
			"votewatch": [{
				"title": null,
				"link": "https://www.votewatch.eu/en/term8-education-in-the-digital-era-challenges-opportunities-and-lessons-for-eu-policy-design-motion-for-re.html",
				"date": "11.12.2018"
			}],
			"background": [{
					"title": "Bericht über das Thema „Bildung im digitalen Zeitalter: Herausforderungen, Chancen und Erkenntnisse für die Gestaltung der EU-Politik“",
					"link": "http://www.europarl.europa.eu/doceo/document/A-8-2018-0400_EN.html?redirect",
					"date": null
				},
				{
					"title": "Zusammenfassung des vom Parlament angenommenen Texts",
					"link": "https://oeil.secure.europarl.europa.eu/oeil/popups/summary.do?id=1566112&t=d&l=de",
					"date": null
				}
			],
			"answers": [{
					"name": "cdu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 25,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 3
						}
					}
				},
				{
					"name": "spd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 22,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 4
						}
					}
				},
				{
					"name": "gruene",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 8,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 2
						}
					}
				},
				{
					"name": "linke",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 5,
							"against": 0,
							"abstained": 0,
							"absent": 2,
							"didntvote": 0
						}
					}
				},
				{
					"name": "afd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 2,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "csu",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 0,
							"abstained": 0,
							"absent": 1,
							"didntvote": 1
						}
					}
				},
				{
					"name": "fdp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 2,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "freie",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "piraten",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "npd",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "lkr",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "oedp",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 0,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 1
						}
					}
				},
				{
					"name": "partei",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "buendnis",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 1,
							"against": 0,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				},
				{
					"name": "independent",
					"voting": {
						"result": null,
						"explanation": null,
						"results": {
							"for": 3,
							"against": 1,
							"abstained": 0,
							"absent": 0,
							"didntvote": 0
						}
					}
				}
			]
		}
	],
	// determines the order of displayed parties 
	// especially relevant if users have the same score with multiple parties
	// in this case: ordered by amount of the 2014 election result
	"parties": [
		{
			"name": "cdu",
			"short_name": "CDU",
			"long_name": "Christlich Demokratische Union Deutschland"
		},
		{
			"name": "spd",
			"short_name": "SPD",
			"long_name": "Sozialdemokratische Partei Deutschlands"
		},
		{
			"name": "gruene",
			"short_name": "Grüne",
			"long_name": "Bündnis 90/Die Grünen"
		},
		{
			"name": "linke",
			"short_name": "Linke",
			"long_name": "DIE LINKE."
		},
		{
			"name": "afd",
			"short_name": "AfD",
			"long_name": "Alternative für Deutschland"
		},
		{
			"name": "csu",
			"short_name": "CSU",
			"long_name": "Christlich-Soziale Union in Bayern e.V."
		},
		{
			"name": "fdp",
			"short_name": "FDP",
			"long_name": "Freie Demokratische Partei"
		},
		{
			"name": "freie",
			"short_name": "Freie Wähler",
			"long_name": "Freie Wähler"
		},
		{
			"name": "piraten",
			"short_name": "Piraten",
			"long_name": "Piratenpartei Deutschland"
		},
		{
			"name": "npd",
			"short_name": "NPD",
			"long_name": "Nationaldemokratische Partei Deutschlands"
		},
		{
			"name": "lkr",
			"short_name": "LKR",
			"long_name": "Liberal-Konservative Reformer"
		},
		{
			"name": "oedp",
			"short_name": "ÖDP",
			"long_name": "Ökologisch-Demokratische Partei"
		},
		{
			"name": "partei",
			"short_name": "Die PARTEI",
			"long_name": "Die PARTEI"
		},
		{
			"name": "buendnis",
			"short_name": "Bündnis C",
			"long_name": "Bündnis C - Christen für Deutschland"
		},
		{
			"name": "independent",
			"short_name": "Parteilos",
			"long_name": "Parteilos"
		}
	]
}